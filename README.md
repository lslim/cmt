# CMT3
This is the working branch of a brand new CMT version.

## Pipenv
From CMT3 we shall use the Pipenv method of creating the virtualenv.

Installation on Fedora:
```
dnf install pipenv
```

Installation on MacOS with homebrew:
```
brew install pipenv
```

Installation on any platform with pip:
```
pip --user install pipenv
```

To create a virtual environment, use `pipenv install  --ignore-pipfile`. This
installs the exact package versions listed in Pipfile.lock. If you omit the
"--ignore-pipfile" flag, Pipfile is used the determine which versions to
download, which do not necessarily match the versions in Pipfile.lock.

Check the following websites for more information on how to use pipenv:
 - https://pipenv.readthedocs.io/en/latest/
 - https://realpython.com/pipenv-guide/
