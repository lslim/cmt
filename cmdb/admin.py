from django.contrib import admin

from cmdb.models import *


class VendorAdmin(admin.ModelAdmin):
    pass
admin.site.register(Vendor, VendorAdmin)


class LocationAdmin(admin.ModelAdmin):
    pass
admin.site.register(Location, LocationAdmin)


class EquipmentMetaAdmin(admin.ModelAdmin):
    pass
admin.site.register(EquipmentMeta, EquipmentMetaAdmin)


class EquipmentModelAdmin(admin.ModelAdmin):
    pass
admin.site.register(EquipmentModel, EquipmentModelAdmin)


class EquipmentRackAdmin(admin.ModelAdmin):
    pass
admin.site.register(EquipmentRack, EquipmentRackAdmin)


class NetworkAdmin(admin.ModelAdmin):
    pass
admin.site.register(Network, NetworkAdmin)


class SubnetAdmin(admin.ModelAdmin):
    pass
admin.site.register(Subnet, SubnetAdmin)


class InterfaceTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(InterfaceType, InterfaceTypeAdmin)


class InterfaceAdmin(admin.ModelAdmin):
    pass
admin.site.register(Interface, InterfaceAdmin)


class EquipmentAdmin(admin.ModelAdmin):
    pass
admin.site.register(Equipment, EquipmentAdmin)