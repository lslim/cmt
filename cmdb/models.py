
from django.db import models
from django.core.exceptions import ValidationError

from hashid_field import HashidAutoField

from cmt_base.tools import get_modules
from dns.models import  Domain
from cmdb import fields


class Vendor(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    home_url = models.URLField()
    support_url = models.URLField()
    support_api = models.CharField(max_length=32, choices=get_modules())


class Location(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)


class EquipmentMeta(models.Model):
    id = HashidAutoField(primary_key=True)
    key = models.CharField(max_length=32)
    value = models.TextField()


class EquipmentModel(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    vendor = models.ForeignKey(Vendor, on_delete=models.DO_NOTHING)


class EquipmentRack(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    location = models.ForeignKey(Location, on_delete=models.DO_NOTHING)


class Network(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)
    vlan = models.IntegerField()

    def __str__(self):
        return 'network-%s' % self.name


class Subnet(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=64, editable=False)
    is_public = models.BooleanField()
    cidr = models.CharField(max_length=32)
    gateway = models.CharField(max_length=32)
    network = models.ForeignKey(Network, on_delete=models.CASCADE)

    def clean(self):

        errors = dict()

        if self.is_public:
            try:
                result = self.__class__.objects.filter(cidr=self.cidr, is_public=True)

                if len(result) > 1 or result[0].id != self.id:
                    errors['cidr'] = ValidationError('Given cidr is already taken', code='invalid')

            except self.DoesNotExist:
                pass

        if errors:
            raise ValidationError(errors)

    def save(self, *args, **kwargs):
        self.name = '%s-%s' % (self.network.name, str(self.cidr))
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class InterfaceType(models.Model):
    id = HashidAutoField(primary_key=True)
    name = models.CharField(max_length=32)


class Interface(models.Model):
    id = HashidAutoField(primary_key=True)
    label = models.CharField(max_length=32)
    domain = models.ForeignKey(Domain, on_delete=models.DO_NOTHING)
    ip = models.GenericIPAddressField()
    aliases = models.CharField(max_length=32, null=True, blank=True)
    hwaddress = models.CharField(max_length=32, null=True, blank=True)
    subnet = models.ForeignKey(Subnet, on_delete=models.DO_NOTHING)
    type = models.ForeignKey(InterfaceType, on_delete=models.DO_NOTHING)


class Equipment(models.Model):
    id = HashidAutoField(primary_key=True)
    skip_in_dns = models.BooleanField()
    label = models.CharField(max_length=32)
    warranty_tag = models.CharField(max_length=32)
    interfaces = models.ForeignKey(Interface, on_delete=models.CASCADE)
    model = models.ForeignKey(EquipmentModel, on_delete=models.DO_NOTHING)
    meta = models.ForeignKey(EquipmentMeta, on_delete=models.CASCADE)
    rack = models.ForeignKey(EquipmentRack, on_delete=models.DO_NOTHING)
    slot = models.IntegerField()
