
from django.db.models.fields import Field
from django.core import exceptions

from IPy import IP


class Cidr(Field):

    def get_internal_type(self):
        return 'Charfield'

    def to_python(self, value):
        try:
            return IP(value)
        except ValueError as error:
            raise exceptions.ValidationError(
                str(error),
                code='invalid_cidr',
                params={'value': value}
            )
